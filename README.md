*******************************************************************
Please take note of the following, in order to able to use JIM.
Jimv2 is built with powershell version 5.1!
Check Powershell Version.
Open a powershell prompt and type the command: $PSVersionTable.PSVersion

#.DISCLAIMER
===================================================================
This SOFTWARE PRODUCT is provided by Alaric Schraven "as is" and "with all faults." Alaric Schraven makes no representations or warranties of any kind concerning the safety, suitability, lack of viruses, inaccuracies, typographical errors, or other harmful components of this SOFTWARE PRODUCT. There are inherent dangers in the use of any software, and you are solely responsible for determining whether this SOFTWARE PRODUCT is compatible with your equipment and other software installed on your equipment. You are also solely responsible for the protection of your equipment and backup of your data, and Alaric Schraven will not be liable for any damages you may suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT.

#.PREREQUISITES
===================================================================
- Microsoft .NET Framework 4.5 or higher
- Windows Management Framework 5.1 (WMF) and can be found:
https://www.microsoft.com/en-us/download/details.aspx?id=54616
Download appropriate 
- Win7-KB3191566-x86.zip or Win7AndW2K8R2-KB3191566-x64.zip


#. USAGE:
===================================================================
Create a shortcut with following command line
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy Bypass -File <full_path>jimv2_ps.ps1 -ConfigFile <full_path>\jim.cfg
--> make it run "as Admin"
--> assign a nice icon or use the one provided :-)
An optional parameter (-force) can be used if Localweb is already included in the (compressed)foundation and not installed as a separate feature;
Sample: 
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy Bypass -File <full_path>jimv2_ps.ps1 -ConfigFile <full_path>\jim.cfg -force

#.POSSIBLE ISSUES
===================================================================
Issue1. A red message saying that PS scripts are not allowed to run because it is disabled!

Explanation:
PowerShell has a number of execution modes that define what type of code it is permitted to run, this is governed by a registry key that lives in the HKLM hive. There are 4 different execution modes, they are:

Restricted:
	Default execution policy, does not run scripts, interactive commands only.
All Signed: 
	Runs scripts; all scripts and configuration files must be signed by a publisher that you trust; opens you to the risk of running signed (but malicious) scripts, after confirming that you trust the publisher.
Remote Signed:
	Local scripts run without signature. Any downloaded scripts need a digital signature, even a UNC path.
Unrestricted:
	Runs scripts; all scripts and configuration files downloaded from communication applications such as Microsoft Outlook, Internet Explorer, Outlook Express and Windows Messenger run after confirming that you understand the file originated from the Internet; no digital signature is required; opens you to the risk of running unsigned, malicious scripts downloaded from these applications

Solution:
Run following command from a powershell depending on the above explanation;
ie: Set-ExecutionPolicy RemoteSigned