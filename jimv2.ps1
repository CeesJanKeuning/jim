<#
.SYNOPSIS
Powershell Script to rename Setting in all related E1 webdevelopment client INI files
To be independend on Oracle Snapshot solution.
Changing/updating all related INI files has the same result with advantage; being much faster and less error prone

.NOTES
    Author: Alaric Schraven
    Last Edit: April 2021
    Version 1.0 - initial release 2017
    Version 2.51 - update April 2021

    Please read Prerequisites and instruction from the Readme.txt!
#>

# Params to be passed into the script (see readme for more information and example)
param (
    [string]$ConfigFile,
    [switch]$force = $false
    )

# Include the file with all related JIM powershell fuctions
. "$PSScriptRoot\jimfunctions.ps1"

# Set / update script verion of JIM accordingly
$JIMVERSION="2.51" # Current version of this file

# --------------------------------------------------------
# -- INIT Variables DO NOT CHANGE!!
# --------------------------------------------------------
$PSVERSION="5.0"                                    # Minimum verion of Powershell (see readme for more information)
$ACTCONSOLE="activConsole.exe"                      # Name of the activeconsole.exe file
$UTB="utb.exe"                                      # Name of utb.exe file
$BASE_REG=setBaseregKey "HKLM:\SOFTWARE"            # Initial Software Registry key 
$BITNESS=setBitness $BASE_REG                       # What bitness is used (32b or 64b)
$JASINI="jas.ini"                                   # Define variable for jas.ini
$JDBJINI="jdbj.ini"                                 # Define variable for jdbj.ini
$JDEINI="jde.ini"                                   # Define variable for jde.ini
$TNS="tnsnames.ora"                                 # Define variable for tnsnames.ora
$INTEROPINI="jdeinterop.ini"                        # Define variable for jdeinterop.ini
$JDELOGPROPS="jdelog.properties"                    # Define variable for jdelog.properties (is also an INI file format)
# Get / set JDE release
$JDEREL=readRegistry "$BASE_REG\OneWorld\Install.ini" "" CurrentRelease 
# Get the folder where client is installed        
$INSTALLPATH=readRegistry "$BASE_REG\OneWorld\Install.ini" $JDEREL InstallPath  
$LOGPATH="$INSTALLPATH\Logs"                        # Define folder for all logfiles (All logfiles will be written here)
$JIMLOG="jim.log"                                   # Name of the logfile for this script
$global:JIMLOGPATH="$LOGPATH\$JIMLOG"               # Global var for the logfile and apath of JIM
$PREVLOGS="$LOGPATH.prev"                           # Backup / logrotation folder
$LOCALWEBLOGPATH="$LOGPATH\Localweb"                # Log directory for localweb
$BSSVLOGPATH="$LOGPATH\BSSV"                        # Log directory for BSSV
$E1LOGPATH="$LOGPATH\E1Log"                         # Log directory for standard JDE logs
$WEBINFDIR=""                                       # define var for directory localweb installation
$ORAINVBACKDIR="$INSTALLPATH\inventory\backup"      # define directory where install gui is dumping backups
$MaxSize=100                                        # define size in MB. If \inventory\backup folder is larger, it will be cleared
$DIRORAINVDEL="N"                                   #
$AUTHOR="Alaric Schraven (asc@cadran.nl)"           # me as the developer :-)
$OS=(Get-WmiObject win32_operatingsystem).caption   # Detect the OS we're running
$PURGEDAYS=30                                       # Nr days older to purge local printqueu
# https://blogs.technet.microsoft.com/heyscriptingguy/2006/12/04/how-can-i-expand-the-width-of-the-windows-powershell-console/
$SCRN_INIT_H=30                                     # set initial cmd Height size (initial menu)
$SCRN_INIT_W=108                                    # set initial cmd Width size
$SCRN_2_H=30                                        # set initial cmd Height size (summary menu)
$SCRN_2_W=108                                       # set initial cmd Width size
$TR9250="9.2.5.0"                                   # Full 4 digit version incl the dots! (TR 9.2.5.0 does not use a local database any more.)
# Set the deployment server name
$DEPServer = readRegistry "$BASE_REG\OneWorld\Install.ini" $JDEREL DeploymentServer
$NO_LOCALDB = "N"                                   # Initial var if a local database installed. Leave N -> NO! variable will be set according detected
 
# --------------------------------------------------------
# -- Setting Variables stops here
# --------------------------------------------------------

# =========================================================
# -- Logic starts below here
# =========================================================


# --------------------------------------------------------
# -- Rotating Logs File Directory Starts here
# --------------------------------------------------------
if (DirExist $LOGPATH) {
    if (DirExist $PREVLOGS) {
        Remove-Item -Recurse -Force $PREVLOGS
    }
    # move Logs folder to Logs.prev
    Move-Item -Path $LOGPATH -Destination $PREVLOGS
}

# open a log file for jim
New-Item -path $LOGPATH -name $JIMLOG -type "file" -value "==== Opening Log for JIM ====`n" -force | Out-Null

LogWrite $JIMLOGPATH "[OS] Running on: $OS"
LogWrite $JIMLOGPATH "[JIM] Base registry key: $BASE_REG"
LogWrite $JIMLOGPATH "[REGISTRY] $JDEREL Install Path found: $INSTALLPATH"
LogWrite $JIMLOGPATH "[JIM] Setting Log path to: $LOGPATH"
LogWrite $JIMLOGPATH "[JDE] Detected bitness: ${BITNESS}bit"

Clear-Host
# set initial window size
try {
    $host.UI.RawUI.WindowSize = 
        New-Object -TypeName System.Management.Automation.Host.Size `
        -ArgumentList ($SCRN_INIT_W, $SCRN_INIT_H) -ErrorAction Stop
        LogWrite $JIMLOGPATH "[OS] Setting cmd window size to (W / H): $SCRN_INIT_W, $SCRN_INIT_H"
} catch {
    LogWrite $JIMLOGPATH "[OS] Error setting cmd windows size to (W / H): $SCRN_INIT_W, $SCRN_INIT_H"
}

# check if parameter file can be found
checkParams $ConfigFile 
LogWrite $JIMLOGPATH "Configfile Ok."

# (re) read the registry
$WEBINFDIR=readRegistry $BASE_REG HTMLWebServer WebInfDir 
if ( ! $WEBINFDIR  ) {
	LogWrite $JIMLOGPATH "[REGISTRY] Webdev feature folder not found in registry: $BASE_REG\HTMLWebServer\WebInfDir"
	LogWrite $JIMLOGPATH "[OS] Test if we can find the folder : $INSTALLPATH\system\JAS\EA_JAS_80.ear\webclient.war."
	$WEBINFDIR = "$INSTALLPATH\system\JAS\EA_JAS_80.ear\webclient.war"
	if (! (DirExist $WEBINFDIR) ) {
		errorMessage "[OS] WebDev Directory Not Found:`n`t`t $WEBINFDIR"
		exit
	} else {
		LogWrite $JIMLOGPATH "[OS] WebDevDir found."
	}
} else {
	LogWrite $JIMLOGPATH "[REGISTRY] Webdev feature folder: $WEBINFDIR."
}



# Patchcodes from registry
[string]$PATHCODEREG=readRegistry "$BASE_REG\OneWorld\Install.ini" $JDEREL Pathcodes
LogWrite $JIMLOGPATH "[REGISTRY] Installed Pathcodes: $PATHCODEREG."
$PATHARR = getUniquePathcodes $PATHCODEREG

LogWrite $JIMLOGPATH "[JIM] Create an Array of the pathcodes and make it unique."
# Create an array with pathcodes and environments from the config file
$ENV_ARR = readCFG "$ConfigFile" "PATH_ENV"
# Update the Environments with valid pathcodes from the registry
$ENV_ARR = cleanUpPaths $PATHARR $ENV_ARR

if ($ENV_ARR.Count -eq 0) {
    # no environment / pathcodes are defined -> error and quit
    errorMessage "`tNo (Valid) Pathcodes defined.`n`t`t`tPlease setup!`n`n`t`t`t(Valid Pathcodes are: $($PATHARR -join ", "))."
    exit
} 

# check if current PS version is same as dev
powershellVersion $PSVERSION
# check if actveconsole is already running and option to kill the process
runningProcess $ACTCONSOLE
# check if UTB is already running and option to kill the process
runningProcess $UTB

# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# BEGIN
# --------------------------------------------------------

$TR=getToolsRelease "$INSTALLPATH\system\bin${BITNESS}\ptf.log"
if ( $TR -eq 0 ) {
	LogWrite $JIMLOGPATH "[JDE] $INSTALLPATH\system\bin${BITNESS}\ptf.log Not found" 
    exit
} else {
	# check TR 
	LogWrite $JIMLOGPATH "[JDE] Comparing detected TR -> $TR with $TR9250"
	$NO_LOCALDB = compareToolsReleases $TR $TR9250
	LogWrite $JIMLOGPATH "[JDE] Detected TR newer than $TR9250 -> $NO_LOCALDB"
	$Message = "[JDE] Local Database installed"
	if ( $NO_LOCALDB -eq "Y" ) { 
		$Message = "[JDE] No Local Database Installed"
	}
	LogWrite $JIMLOGPATH $Message
}

# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# END
# --------------------------------------------------------

$APPNAME="JDE INI MANAGER (aka JIM) - EnterpriseOne Environments - Version $JIMVERSION`n`t`tJDEdwards $JDEREL (ToolsRelease $TR, ${BITNESS}bit)"
$Host.UI.RawUI.WindowTitle = "JDE INI MANAGER (aka JIM) - Version $JIMVERSION -- by $AUTHOR"
$FULL_JDEINI="$env:windir\$JDEINI"
if (! (FileExist $FULL_JDEINI)) {
    errorMessage "JDE.INI not found at: $env:windir"
    exit
}
$FULL_JASINI="$WEBINFDIR\WEB-INF\classes\$JASINI"
if (! (FileExist $FULL_JASINI)) {
    errorMessage "JAS.INI not found at: $FULL_JASINI"
    exit
}
$FULL_JDBJINI="$WEBINFDIR\WEB-INF\classes\$JDBJINI"
if (! (FileExist $FULL_JDBJINI)) {
    errorMessage "JDBJ.INI not found at: $FULL_JDBJINI"
    exit
}
$FULL_JDELOGPROPS="$WEBINFDIR\WEB-INF\classes\$JDELOGPROPS"
if (! (FileExist $FULL_JDELOGPROPS)) {
    errorMessage "JDELOG.PROPERTIES not found at: $FULL_JDELOGPROPS"
    exit
}
$FULL_E1LOGPROPS="$INSTALLPATH\system\classes\$JDELOGPROPS"
$FULL_BSSVLOGPROPS=""
$FULL_INTEROPINI=""
# read jde.ini file in memory
$iniContent = GetIniContent "$FULL_JDEINI"
# get the database type. In case Oracle we need to do something with tnsnames.ora
$DBTYPE = $iniContent["DB SYSTEM SETTINGS"]["Type"]
LogWrite $JIMLOGPATH "[JDE] DB Type from jde.ini: $DBTYPE"
[int]$jdecon=$iniContent["JDENET"]["serviceNameListen"]
LogWrite $JIMLOGPATH "[JDE] Listenport from jde.ini: $jdecon"
$SECSVR=$iniContent["SECURITY"]["SecurityServer"]
LogWrite $JIMLOGPATH "[JDE] Security Server from jde.ini: $SECSVR"

# show a menu to the user
LogWrite $JIMLOGPATH "[JIM] Starting the menu."
LoadMenuSystem $ENV_ARR $APPNAME #returns an array (pathcode, Environment)
# script timing :-) Just curious
$sw = [Diagnostics.Stopwatch]::StartNew()
$SEL_ENV=$ENV_ARR[$user_choice][1]     # easy referencing
$SEL_PATH=$ENV_ARR[$user_choice][0]    # easy referencing
LogWrite $JIMLOGPATH "[JIM] Continue using user input. ENV=$SEL_ENV, PATH=$SEL_PATH"

$BASE_PACK = readRegistry "$BASE_REG\OneWorld\Install.ini" $JDEREL BasePackage
$PATH_PACK = readRegistry "$BASE_REG\OneWorld\Install.ini" $JDEREL\$SEL_PATH BasePackageName


LogWrite $JIMLOGPATH "[JIM] Found Base package (latest systemcomp): $BASE_PACK"
LogWrite $JIMLOGPATH "[JIM] Found Pathcode package (latest Full): $PATH_PACK"

# set path to jdeinterop.ini only if <pathcode>\ini\sbf\ exists. Will only exist when developing BSSV
if (DirExist "$INSTALLPATH\$SEL_PATH\ini\sbf") {
    LogWrite $JIMLOGPATH "[OS] Directory found: $INSTALLPATH\$SEL_PATH\ini\sbf. -> BSSV may be developed / built."
    $FULL_INTEROPINI="$INSTALLPATH\$SEL_PATH\ini\sbf\$INTEROPINI"
    if (! (FileExist $FULL_INTEROPINI)) {
		Copy-Item "\\$DEPServer\$JDEREL\$SEL_PATH\ini\sbf\$INTEROPINI" -Destination $FULL_INTEROPINI
		
        #errorMessage "JDEINTEROP.INI not found at: $FULL_INTEROPINI"
        #exit
    }
    $FULL_BSSVLOGPROPS="$INSTALLPATH\$SEL_PATH\ini\sbf\$JDELOGPROPS"
    if (! (FileExist $FULL_BSSVLOGPROPS)) {
		Copy-Item "\\$DEPServer\$JDEREL\$SEL_PATH\ini\sbf\$JDELOGPROPS" -Destination $FULL_BSSVLOGPROPS
        #errorMessage "JDELOG.PROPERTIES not found at: $FULL_BSSVLOGPROPS"
        #exit
    }

    # --------------------------------------------------------
    # -- Updating JDEINTEROP.INI Start here
    # --------------------------------------------------------
    LogWrite $JIMLOGPATH "[JDE] Start Updating JDEINTEROP.INI"
    [int]$jdelist=$jdecon+100
    SetIniValue $FULL_INTEROPINI "JDENET" "serviceNameConnect" "$jdecon"
    SetIniValue $FULL_INTEROPINI "JDENET" "serviceNameListen" "$jdelist"
    SetIniValue $FULL_INTEROPINI "SECURITY" "SecurityServer" "$SECSVR"  
    # writing the log
    LogWrite $JIMLOGPATH "[JDE] Updated JDEINTEROP.INI: [JDENET] serviceNameConnect: $jdecon"
    LogWrite $JIMLOGPATH "[JDE] Updated JDEINTEROP.INI: [JDENET] serviceNameListen: $jdelist"
    LogWrite $JIMLOGPATH "[JDE] Updated JDEINTEROP.INI: [SECURITY] SecurityServer: $SECSVR"
    # --------------------------------------------------------
    # -- Updating JDEINTEROP.INI Ends here
    # --------------------------------------------------------
} else {
    LogWrite $JIMLOGPATH "[OS] Directory not found: $INSTALLPATH\$SEL_PATH\ini\sbf. -> Assuming no BSSV will be developed / built."
}

# --------------------------------------------------------
# -- Updating JDE.INI Start here
# --------------------------------------------------------
LogWrite $JIMLOGPATH "[JDE] Start Updating JDE.INI"
SetIniValue $FULL_JDEINI "SVR" "EnvironmentName" "$SEL_ENV"
SetIniValue $FULL_JDEINI "SVR" "LibraryListName" "$SEL_PATH"
SetIniValue $FULL_JDEINI "DB SYSTEM SETTINGS" "Default Env" "$SEL_ENV"
SetIniValue $FULL_JDEINI "DB SYSTEM SETTINGS" "Default PathCode" "$SEL_PATH"
if ( $NO_LOCALDB -eq "Y" ){
	SetIniValue $FULL_JDEINI "DB SYSTEM SETTINGS - SECONDARY" "Base Datasource" ""
	SetIniValue $FULL_JDEINI "DB SYSTEM SETTINGS - SECONDARY" "DatabaseName2" ""
} else {
	SetIniValue $FULL_JDEINI "DB SYSTEM SETTINGS - SECONDARY" "Base Datasource" "Local - $($SEL_PATH)"
	SetIniValue $FULL_JDEINI "DB SYSTEM SETTINGS - SECONDARY" "DatabaseName2" "JDELOCAL_$($SEL_PATH)"
}
SetIniValue $FULL_JDEINI "REPLICATION" "DefaultEnvironment" "$SEL_ENV"
SetIniValue $FULL_JDEINI "SECURITY" "DefaultEnvironment" "$SEL_ENV"

# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# BEGIN
# --------------------------------------------------------

if ( $NO_LOCALDB -eq "Y" ){ 
	SetIniValue $FULL_JDEINI "LOCALWEB" "Spec Datasource" ""
} else {
	SetIniValue $FULL_JDEINI "LOCALWEB" "Spec Datasource" "Local - $($SEL_PATH)"
}

# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# END
# --------------------------------------------------------

SetIniValue $FULL_JDEINI "SIGNON" "LASTENV" "$SEL_ENV"
# writing the log
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [SVR] EnvironmentName: $SEL_ENV"
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [SVR] LibraryListName: $SEL_PATH"
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DB SYSTEM SETTINGS] Default Env: $jdecon"
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DB SYSTEM SETTINGS] Default PathCode: $jdecon"
if ( $NO_LOCALDB -eq "Y" ){
	LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DB SYSTEM SETTINGS - SECONDARY] Base Datasource: "
	LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DB SYSTEM SETTINGS - SECONDARY] DatabaseName2: "
} else {
	LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DB SYSTEM SETTINGS - SECONDARY] Base Datasource: Local - $($SEL_PATH)"
	LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DB SYSTEM SETTINGS - SECONDARY] DatabaseName2: JDELOCAL_$($SEL_PATH)"
}

LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [REPLICATION] DefaultEnvironment: $SEL_ENV"
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [SECURITY] DefaultEnvironment: $SEL_ENV"

# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# BEGIN
# --------------------------------------------------------
if ( $NO_LOCALDB -eq "Y" ){ 
	LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [LOCALWEB] Spec Datasource:"
} else {
	LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [LOCALWEB] Spec Datasource:Local - $($SEL_PATH)"
}
# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# END
# --------------------------------------------------------

LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [SIGNON] LASTENV: $SEL_ENV"
# --------------------------------------------------------
# -- Updating JDE.INI Ends here
# --------------------------------------------------------

# --------------------------------------------------------
# -- Updating JAS.INI Start here
# --------------------------------------------------------
LogWrite $JIMLOGPATH "[JDE] Start Updating JAS.INI"
[int]$jdelist=$jdecon+200
SetIniValue $FULL_JASINI "OWWEB" "PathCodes" "('$SEL_PATH')"
SetIniValue $FULL_JASINI "JDENET" "serviceNameConnect" "$jdecon"
SetIniValue $FULL_JASINI "JDENET" "serviceNameListen" "$jdelist"
SetIniValue $FULL_JASINI "JDENET" "tempFileDir" "$env:TEMP"
SetIniValue $FULL_JASINI "SERVER" "glossaryTextServer" "$($SECSVR):$($jdecon)"
SetIniValue $FULL_JASINI "SECURITY" "SecurityServer" "$SECSVR"
# Wrinting the log
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [OWWEB] PathCodes: ('$SEL_PATH')"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [JDENET] serviceNameConnect: $jdecon"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [JDENET] serviceNameListen: $jdelist"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [JDENET] tempFileDir: $env:TEMP"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [SERVER] glossaryTextServer: $($SECSVR):$($jdecon)"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [SECURITY] SecurityServer: $SECSVR"
# --------------------------------------------------------
# -- Updating JAS.INI Ends here
# --------------------------------------------------------

# --------------------------------------------------------
# -- Updating JDBJ.INI Start here
# --------------------------------------------------------
LogWrite $JIMLOGPATH "[JDE] Start Updating JDBJ.INI"
SetIniValue $FULL_JDBJINI "JDBj-BOOTSTRAP SESSION" "environment" "$SEL_ENV"

# --------------------------------------------------------
#-- change the log file depend on the tools release #Oday Rafeh# BEGIN
# --------------------------------------------------------

if ( $NO_LOCALDB -eq "Y" ){ 
	SetIniValue $FULL_JDBJINI "JDBj-SPEC DATA SOURCE" "name" ""
	SetIniValue $FULL_JDBJINI "JDBj-SPEC DATA SOURCE" "physicalDatabase" ""
} else {
	SetIniValue $FULL_JDBJINI "JDBj-SPEC DATA SOURCE" "name" "Local - $($SEL_PATH)"
	SetIniValue $FULL_JDBJINI "JDBj-SPEC DATA SOURCE" "physicalDatabase" "JDELOCAL_$($SEL_PATH)"
}
# --------------------------------------------------------
# -- change the log file depend on the tools release #Oday Rafeh# END
# --------------------------------------------------------

#Writing the log
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [JDBj-BOOTSTRAP SESSION] environment: $SEL_ENV"

# --------------------------------------------------------
# -- change the log file depend on the tools release #Oday Rafeh# BEGIN
# --------------------------------------------------------

if ( $NO_LOCALDB -eq "Y" ) {
	LogWrite $JIMLOGPATH "[JDE] Updated JDBJ.INI: [JDBj-SPEC DATA SOURCE] name: "
	LogWrite $JIMLOGPATH "[JDE] Updated JDBJ.INI: [JDBj-SPEC DATA SOURCE] physicalDatabase:"
}else{
	LogWrite $JIMLOGPATH "[JDE] Updated JDBJ.INI: [JDBj-SPEC DATA SOURCE] name: Local - $($SEL_PATH)"
	LogWrite $JIMLOGPATH "[JDE] Updated JDBJ.INI: [JDBj-SPEC DATA SOURCE] physicalDatabase: JDELOCAL_$($SEL_PATH)"
}
# --------------------------------------------------------
# -- change the log file depend on the tools release #Oday Rafeh# END
# --------------------------------------------------------

# --------------------------------------------------------
# -- Updating JDBJ.INI Ends here
# --------------------------------------------------------

# Copy tnsnames.ora (in case Oracle DB), JAS.INI and JDBJ.INI 
if ($FULL_INTEROPINI) {
    # copy JAS.INI is NOT needed, causes issue
    # removed,  copyFile "$FULL_JASINI" "$INSTALLPATH\$SEL_PATH\ini\sbf\"
    # copy JDBJ.INI
    copyFile $FULL_JDBJINI "$INSTALLPATH\$SEL_PATH\ini\sbf\"
    LogWrite $JIMLOGPATH "[JDE] Copying $FULL_JDBJINI to $INSTALLPATH\$SEL_PATH\ini\sbf\"
    if ($DBTYPE -eq "O") { 
        LogWrite $JIMLOGPATH "Oracle database thus copying also TNSNAMES"
        LogWrite $JIMLOGPATH "[JDE] Copying $WEBINFDIR\WEB-INF\classes\$TNS to $INSTALLPATH\$SEL_PATH\ini\sbf\"
        copyFile "$WEBINFDIR\WEB-INF\classes\$TNS" "$INSTALLPATH\$SEL_PATH\ini\sbf\"
    }
}


# Create the new Log files directories
if ($FULL_INTEROPINI) {
    New-Item -ItemType Directory -Force -Path  "$BSSVLOGPATH" | Out-Null
}
New-Item -ItemType Directory -Force -Path  "$LOCALWEBLOGPATH" | Out-Null
New-Item -ItemType Directory -Force -Path  "$E1LOGPATH" | Out-Null
# --------------------------------------------------------
# -- Rotating Logs File Directory Ends here
# --------------------------------------------------------

# --------------------------------------------------------
# -- Delete Inventory\backup Directory Starts here
# --------------------------------------------------------
if (DirExist $ORAINVBACKDIR) {
    # check dir size
    $DIRSIZE=getDirSize $ORAINVBACKDIR
    $rounded=[math]::Round($DIRSIZE,2)
    LogWrite $JIMLOGPATH "[OS] Oracle Inventory Backup Directory size: $rounded MB"
    if ($DIRSIZE -gt $MaxSize) {
        Clear-Host
        Write-Host "`t`t==================================================================="
        Write-Host "`t`tDir size $ORAINVBACKDIR is: " -NoNewline
        write-host "$rounded MB" -fore Yellow
        Write-Host
        Write-Host "`t`tPlease wait while cleaning the directory..."
        Write-Host "`t`t==================================================================="
        #Read-Host "`tPress Enter to exit..." | Out-Null
        Remove-Item -Path "$ORAINVBACKDIR\*" -Recurse -Force
        $DIRORAINVDEL="Y"
        LogWrite $JIMLOGPATH "[OS] Oracle Inventory Backup Directory cleaned."
    }
}
# --------------------------------------------------------
# -- Delete Inventory\backup Directory Ends here
# --------------------------------------------------------

# cleanup printqueue
#Get-ChildItem "$INSTALLPATH\PrintQueue\" -Recurse -File | Where-Object CreationTime -lt  (Get-Date).AddDays(-$PURGEDAYS)  | Remove-Item -Force
If(test-path $INSTALLPATH\PrintQueue\)
{
    LogWrite $JIMLOGPATH "[OS] Cleaning up: $INSTALLPATH\PrintQueue\ (files older than $PURGEDAYS)"
    deleteFileswithProgress "$INSTALLPATH\PrintQueue\" $PURGEDAYS
}


# --------------------------------------------------------
# -- Updating INI Files LOG file location Starts here
# --------------------------------------------------------
# Get all logfiles together at single location under install directory
If(!(test-path $LOCALWEBLOGPATH))
{
    New-Item -ItemType Directory -Force -Path  $LOCALWEBLOGPATH | Out-Null
}
# set ini files loglocation to config paths to $LOGPATH
# jdelog.properties
# ===== LOCALWEB LOGS
# enable debug log section LOG2 (19-2-2018)
(Get-Content $FULL_JDELOGPROPS) | Foreach-Object {$_ -replace '(#*\[LOG2\])','[LOG2]'}  | Out-File $FULL_JDELOGPROPS

SetIniValue $FULL_JDELOGPROPS "E1LOG" "FILE" "$LOCALWEBLOGPATH\jderoot.log"
SetIniValue $FULL_JDELOGPROPS "LOG1" "FILE" "$LOCALWEBLOGPATH\jas.log"
LogWrite $JIMLOGPATH "[JDE] Updated JDELOG.PROPERTIES: [E1LOG] FILE: $LOCALWEBLOGPATH\jderoot.log"
LogWrite $JIMLOGPATH "[JDE] Updated JDELOG.PROPERTIES: [LOG1] FILE: $LOCALWEBLOGPATH\jas.log"
# jas.ini
SetIniValue $FULL_JASINI "LOGS" "rtlog" "$LOCALWEBLOGPATH\rt.log"
SetIniValue $FULL_JASINI "LOGS" "rtdebug" "$LOCALWEBLOGPATH\rtdebug.log"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [LOGS] rtlog: $LOCALWEBLOGPATH\rt.log"
LogWrite $JIMLOGPATH "[JDE] Updated JAS.INI: [LOGS] rtdebug: $LOCALWEBLOGPATH\trdebug.log"
# jde.ini
# ===== Standard JDE.LOGS
SetIniValue $FULL_JDEINI "DEBUG" "DebugFile" "$LOGPATH\jdedebug.log"
SetIniValue $FULL_JDEINI "DEBUG" "JobFile" "$LOGPATH\jde.log"
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DEBUG] DebugFile: $LOGPATH\jdedebug.log"
LogWrite $JIMLOGPATH "[JDE] Updated JDE.INI: [DEBUG] JobFile: $LOGPATH\jde.log"
# ===== E1 Java Logs
SetIniValue $FULL_E1LOGPROPS "E1LOG" "FILE" "$E1LOGPATH\e1root.log"
SetIniValue $FULL_E1LOGPROPS "LOG1" "FILE" "$E1LOGPATH\e1jas.log"
LogWrite $JIMLOGPATH "[JDE] Updated E1LOG.PROPERTIES: [E1LOG] DebugFile: $E1LOGPATH\e1root.log"
LogWrite $JIMLOGPATH "[JDE] Updated E1LOG.PROPERTIES: [LOG1] JobFile: $E1LOGPATH\e1jas.log"


# ===== BSSV LOGS
if ($FULL_INTEROPINI) {
    New-Item -ItemType Directory -Force -Path  "$BSSVLOGPATH" | Out-Null
    SetIniValue $FULL_BSSVLOGPROPS "E1LOG" "FILE" "$BSSVLOGPATH\jderoot.log"
    SetIniValue $FULL_BSSVLOGPROPS "JAS" "FILE" "$BSSVLOGPATH\jas.log"
    SetIniValue $FULL_BSSVLOGPROPS "BSSV" "FILE" "$BSSVLOGPATH\bssv.log"
    LogWrite $JIMLOGPATH "[JDE] Updated JDELOG.PROPERTIES: [E1LOG] FILE: $BSSVLOGPATH\jderoot.log"
    LogWrite $JIMLOGPATH "[JDE] Updated JDELOG.PROPERTIES: [JAS] FILE: $BSSVLOGPATH\jas.log"
    LogWrite $JIMLOGPATH "[JDE] Updated JDELOG.PROPERTIES: [BSSV] FILE: $BSSVLOGPATH\bssv.log"
}
# --------------------------------------------------------
# -- Updating INI Files LOG file location Ends here
# --------------------------------------------------------

# End stopwatch
$sw.Stop()
if ($DIRORAINVDEL -eq "Y") {
    #resize windows to fit extra information about the deletion of the inventory folder
    try {
        $host.UI.RawUI.WindowSize = 
            New-Object -TypeName System.Management.Automation.Host.Size `
            -ArgumentList ($SCRN_2_W, $SCRN_2_H) -ErrorAction Stop
            LogWrite $JIMLOGPATH "[OS] Setting cmd window size to (W / H): $SCRN_2_W, $SCRN_2_H"
    } catch {
        LogWrite $JIMLOGPATH "[OS] Error setting cmd windows size to (W / H): $SCRN_2_W, $SCRN_2_H"
    }
}
LogWrite $JIMLOGPATH "[JIM] Show user summary screen, and wait for user input to continue."
showSummary

Read-Host "`tPress Enter to start ActiveConsole.exe..." | Out-Null

#start Activeconsole.exe "run as admin"

Start-Process -FilePath "$INSTALLPATH\system\bin${BITNESS}\$ACTCONSOLE" -Verb runAs
LogWrite $JIMLOGPATH "[JIM] Started Activeconsole.exe. (quit script)"
Exit
