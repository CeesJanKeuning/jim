<#
.SYNOPSIS
This script is the function labrary for the min JIM.ps1 script. 
Without this library JIM does not work

.NOTES
    Author: Alaric Schraven
    Last Edit: April 2021
    Version 1.0 - initial release 2017
    Version 1.1 - update April 2021
#>

# --------------------------------------------------------
# -- Function section starts below here
# --------------------------------------------------------

# :: FUNCTION Compare two TR numbers
# :: param 1: string 1 -> Detected TR (ptf.log)
# :: param 2: string 2 -> TR9250
function compareToolsReleases ($str1, $str2) {
	[int]$str1 = $str1 -replace '[.]',''
	[int]$str2 = $str2 -replace '[.]',''
	$ret = "N"
	if ( $str1 -ge $str2 ) {
		$ret = "Y"
	}
	return $ret
}

# :: FUNCTION Check JDE install registry key
# :: Param 1: base key
# :: 32bit JDE at "HKLM:\SOFTWARE\Wow6432Node\JDEdwards"
# :: 64bit JDE at "HKLM:\SOFTWARE\JDEdwards"
function setBaseregKey ($baseRegKey) {
    if (Test-Path "$baseRegKey\JDEdwards") {
        return "$baseRegKey\JDEdwards"
    } elseif (Test-Path "$baseRegKey\Wow6432Node\JDEdwards") {
        return "$baseRegKey\Wow6432Node\JDEdwards"
    } else {
        errorMessage "Registry key not found.`n`n`t`t`tPlease check if JDE client is installed."
    }
}

# :: FUNCTION Check JDE bitness 32b or 64b
# :: param 1: baseregistry key
function setBitness ($regpath) {
    $bn = "64"
    if ($regpath -match "Wow6432Node") {
        $bn = "32"
    } 
    return $bn
}

# :: FUNCTION Check script parameters
# :: Param 1: full filepath to jim.cfg
function checkParams ($ConfigFile) {  
    if ($ConfigFile) {
        if (!(Test-Path "$ConfigFile")) {
            errorMessage "JIM Config File not found at:`n`t`t`t$ConfigFile.`n`n`t`t`tPlease supply correct filepath!"	
            exit
        }
        Write-host
        Write-host "Reading JIM Configfile: $ConfigFile"
		LogWrite $JIMLOGPATH "[JIM] Reading Configfile: $ConfigFile"
        Start-Sleep -m 200
    } else {
        errorMessage "Please provide a path and filename to jim.cfg as an argument.`n`n`t`t`tie. D:\E9nn\jim\jim.cfg"
        exit
    }
}

# :: FUNCTION get a current date / timestamp
function Get-TimeStamp {
    return "[{0:dd-MMM-yyyy} {0:HH:mm:ss}]" -f (Get-Date)  
}

# :: FUNCTION write messages to logfile
# :: param 1: full logfile plus path
# :: param 2: logvalue 
Function LogWrite ($logfile, $logstring)
{
	$dt = (Get-Date).tostring("dd-MMM-yyyy HH:mm:ss:fff")
	Add-content -path  $logfile -value "[$dt] $logstring"
}

# :: FUNCTION Read a given registry value
# :: param 1: base registry key 
# :: param 2: registry entry
# :: param 3: the key to be read
function readRegistry ($regBase,$regKey,$valueName) {
    # Param (
    #     [string]$regBase,
    #     [string]$regKey,
    #     [string]$valueName
    # )
    $res = test-path -Path $regBase\$regKey 
    if ( $res ) {
        $res = Get-ItemPropertyValue -Path $regBase\$regKey -Name $valueName
        return $res
    } else {
        return $false
    }
}

# :: FUNCTION write a given registry value
# :: param 1: base registry key 
function writeRegistryPath ($regPath) {
    New-Item -Path $regPath -Force | Out-Null
}

# :: FUNCTION write a given registry value
# :: param 1: base registry key 
# :: param 2: registry entry
# :: param 3: the key to be read
function writeRegistryValue ($regPath, $regName, $regValue) {
    New-ItemProperty -Path $regPath -Name $regName -Value $regValue -PropertyType STRING -Force | Out-Null
}

# :: FUNCTION Read jim.cfg and put pathcode and environments in array
# :: Param 1: filepath to jim config file
# :: Param 2: string to recognise pathcode/environment
function readCFG ($configFile, $line2recogn) {
    #Write-Host "readcfg: $configFile, $line2recogn"
    $file=Get-Content $configFile
    $ARR=@()
    foreach ($line in $file) {
        # cleanup the lin eof text
        $l=$line.toUpper().trim()
        if ($l.StartsWith($line2recogn)) {
            $text,$pathenv=$l.split("=")
            if ($pathenv -match "\w") {
                $path,$env=$pathenv.split(",")
                $ARR +=,@($path,$env)
            }
        }
    }  
    # convert to ArryList
    return $ARR
}


##############################
#.SYNOPSIS
#Functio to read INI file content
#
#.DESCRIPTION
#https://blogs.technet.microsoft.com/heyscriptingguy/2011/08/20/use-powershell-to-work-with-any-ini-file/
#
#.PARAMETER filePath
#Parameter description
#
#.EXAMPLE
# $iniContent = Get-IniContent c:\temp\file.ini
# $iniContent[386Enh]
# $value = $iniContent[386Enh][EGA80WOA.FON
# $iniContent[386Enh].Keys | %{$iniContent["386Enh"][$_]}
#
##############################
function GetIniContent ($filePath) {
    $ini = @{}
    switch -regex -file $FilePath
    {
        "^\[(.+)\]" # Section
        {
            $section = $matches[1]
            $ini[$section] = @{}
            $CommentCount = 0
        }
        "^(;.*)$" # Comment
        {
            if (!($section))  
            {  
                $section = "No-Section"  
                $ini[$section] = @{}  
            }  
            $value = $matches[1]
            $CommentCount = $CommentCount + 1
            $name = "Comment" + $CommentCount
            $ini[$section][$name] = $value
        }
        "(.+?)\s*=(.*)" # Key
        {
            if (!($section))  
            {  
                $section = "No-Section"  
                $ini[$section] = @{}  
            }  
            $name,$value = $matches[1..2]
            $ini[$section][$name] = $value
        }
    }
    return $ini
}

# Ini files
# Replacing a value in a key inside a section sample
#
# Calling Windows API function WritePrivateProfileString
# And using some C# code
# https://gist.github.com/jatubio/90cda6b32c0b19543a74
# http://stackoverflow.com/questions/29688299/powershell-and-regex-how-to-replace-a-ini-name-value-pair-inside-a-named-sectio/29688435#29688435
$source = @"
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
public static class IniFile
{
    [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool WritePrivateProfileString(string lpAppName,
       string lpKeyName, string lpString, string lpFileName);
    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
    static extern uint GetPrivateProfileString(
       string lpAppName,
       string lpKeyName,
       string lpDefault,
       StringBuilder lpReturnedString,
       uint nSize,
       string lpFileName);
    public static void WriteValue(string filePath, string section, string key, string value)
    {
        string fullPath = Path.GetFullPath(filePath);
        bool result = WritePrivateProfileString(section, key, value, fullPath);
    }
    public static string GetValue(string filePath, string section, string key, string defaultValue)
    {
        string fullPath = Path.GetFullPath(filePath);
        StringBuilder sb = new StringBuilder(500);
        GetPrivateProfileString(section, key, defaultValue, sb, (uint)sb.Capacity, fullPath);
        return sb.ToString();
    }
}
"@

Add-Type -TypeDefinition $source

# :: FUNCTION Writing INI Values (file [section] key value)
# :: Param 1: path and filename
# :: Param 2: ini secton
# :: Param 3: ini key within section
# :: Param 4: ini value of the key
function SetIniValue {
    param (
    [string]$path,
    [string]$section,
    [string]$key,
    [string]$value
    )

    $fullPath = [System.IO.Path]::GetFullPath($path)
    [IniFile]::WriteValue($fullPath, $section, $key, $value)
}

# :: FUNCTION Get Client Tools release
# :: param 1: e1 install path
function getToolsRelease ($filepath)  {
    $tr = 0
    if (FileExist $filepath) {
        $tr = get-Content -Path $filepath | Select-String -Pattern '^[0-9].'
        LogWrite $JIMLOGPATH "[JDE] Tools release: $tr"             
    } else {
        errorMessage "Failed to read $filepath. (not found)"
    }
    return $tr
}

# :: FUNCTION Create The user menu
# :: param 1: Array with availbale choices
# :: param 2: version
# :: param 3: author
function LoadMenuSystem ($envArr, $appName) {

    $Message = ""
    $Continue = $true

    do {
        Clear-Host
        #â€¦ Present the Menu Options
		Write-Host "`n`t$appName`n" -ForegroundColor Magenta
        Write-Host "`t`tPlease select the Environment you wish to use:`n" -Fore Cyan
        Write-Host ''
    
        if ($Message -ne '') {
            Write-Host ''
        }

        $i=1
        foreach ($Item in @($envArr)) {
            # loop through array
           Write-Host "`t`t$i. Environment: " -NoNewline
           Write-Host $Item[1] -fore Yellow -NoNewline
           Write-Host "`t(Pathcode:" $Item[0]")"
           $i++
        }
        Write-Host ''
        Write-Host "`t`t`Q. Quit menu."
        Write-Host ''
    
        $Message = ''
        $Choice = Read-Host "`tSelect option"
        if ($Choice -eq 'Q'){
            $Continue = $false
            exit
        } #this will release script from DO/WHILE loop.
    
        if($Choice -lt $envArr.Count+1 -and $Choice -gt 0) {
            Clear-Host 
            Write-Host "Starting update INI files..." $envArr[$Choice-1][1]
            $var=$Choice-1
            LogWrite $JIMLOGPATH "[JIM] User typed option: $var"
            Set-Variable -Name user_choice -Value $var -Scope global
            $Continue = $false
        } else {
            $Message = 'Invalid Selection, try again' 
        }
        if ($Continue) {
            if ($Message -ne '') {
                Write-Host $Message -BackgroundColor Black -ForegroundColor Red
            }
            Read-Host 'Hit any key to continue'
        }
        Clear-Host
    }
    WHILE ($Continue)
    return
}


# :: FUNCTION check if activeconsole is already running
# :: param 1: process name
function runningProcess ($exeName) {
    $exeName = $exeName.Substring(0,$exeName.LastIndexOf('.')).split('\')[-1]
    $isRunning = Get-Process | Where-Object { $_.Name -eq "$exeName" } | Select-Object -First 1
    if ($isRunning) {
        Clear-Host
        Write-Host "`t`t============================================================"
        Write-Host "`t`t`t`tFound $exeName already running!" -Fore Red
        Write-Host "`t`t============================================================"
        #Write-Host "`t`tKill the process?:`n" -Fore Cyan
        #Write-Host ''
        $confirmation = Read-Host "`t`tKill the process?:" 
        if ($confirmation -eq 'y') {
            Get-Process | Where-Object { $_.Name -eq "$exeName" } | Select-Object -First 1 | Stop-Process
        }
    }
}

# :: FUNCTION check if current TR is 9.1.5 or above
# :: above localweb is WLS and under is OC4J
# :: param 1: tools release
function CheckToolsRelease {
    param ([string]$tr)
    $isTrue = $false
    $tra=$tr.replace(".","")
    if ($tra -ge 915) {
        $isTrue = $true
    }
    return $isTrue
}

# :: FUNCTION check Powershell version
function powershellVersion ($minVer){
    $curVer=$PSVersionTable.PSVersion -replace "[.]","."
    LogWrite $JIMLOGPATH "[OS] Installed Powershell version: $curVer."
    $curVer=($PSVersionTable.PSVersion -replace "[.]","").Substring(0,2)
    $minVer=($minVer -replace "[.]","").Substring(0,2)
    if ($curVer -lt $minVer ) {
        LogWrite $JIMLOGPATH "Powershell version: $curVer is not supported (min. $minVer)"
        Clear-Host
        Write-Host "`t`t==================================================================="
        Write-Host "`t`t`tYou are running Powershell version: $($PSVersionTable.PSVersion) " -fore Red
        Write-Host "`t`t`twhich is not verified!" -Fore Red
        Write-Host
        Write-Host "`t`t`tPlease read the readme.txt." -Fore Cyan
        Write-Host "`t`t==================================================================="
        $confirmation = Read-Host "`t`tContinue? (N to exit):" 
        if ($confirmation -eq 'n') {
            exit
        }
    }
}

# :: FUNCTION displaying error message
# :: Param 1: message string to display
function errorMessage ($message) {
    LogWrite $JIMLOGPATH "$message. Exit Script."
    Clear-Host
    Write-Host "`t`t==================================================================="
    Write-Host "`t`t$message" -fore Red
    Write-Host "`t`t==================================================================="
    Read-Host "`tPress Enter to exit..." | Out-Null
}

# :: FUNCTION test if directory exists
# :: param 1: path to dir
function DirExist ($path) {
    $retval=$false
    if ( Test-Path "$path" -PathType Container ) {
        $retval=$true
    }
    return $retval
}

# :: FUNCTION test if file exists
# :: param 1: path to file
function FileExist ($filePath) {
    $retval=$false
    if ( Test-Path "$filePath" -PathType Leaf ) {
        LogWrite $JIMLOGPATH "[OS] File found: $filePath"
        $retval=$true
    } 
    return $retval
}

# :: FUNCTION Copy file
# Param 1: full source directory incl file
# Param 2: full target directory
function copyFile ($src, $trgt) {
    Copy-Item $src $trgt
}

# :: FUNCTION Copy file
# Param 1: full source directory
# return size in MB
function getDirSize ($dir) {
	return (Get-ChildItem $dir -recurse | Measure-Object -property length -sum).sum /1MB
}

# :: FUNCTION get unique pathcodes 
# :: Param 1: pathcodes as string
function getUniquePathcodes ($pathcodes) {
    $PATHARR=$pathcodes.Replace(" ","") -split ","    # convert string to array
    $PATHARR=$PATHARR | Sort-Object | Get-Unique        # remove possible dupliacte entries
    return $PATHARR
}

# :: FUNCTION remove user defined pathcodes that do not exist
# :: Param 1: Array With user defined pathcodes
# :: Param 2: Array with existing pathcodes
function cleanUpPaths ($existArr, $usrArr ){
    $env_list_temp=@()
    foreach ($pc in $usrArr) {
        foreach($item in $existArr) {
            if ($pc[0] -eq $item){
                $env_list_temp +=,($pc)
            }
        }
    }
    return , $env_list_temp
}

# :: FUNTION Summary
# :: Show summary to user
function showSummary {
    ###Clear-Host
    Write-Host
	Write-Host "`t========================================>> SUMMARY <<========================================" -Fore Yellow
    #Write-Host "`t=============================================================================" -Fore Yellow
    Write-Host
    Write-Host "`t - Updated the INI files to use: (Processed in $($sw.Elapsed.TotalMilliSeconds) milliseconds)"
    Write-Host "`t`tPathcode: " -NoNewline
    Write-Host "`t$SEL_PATH" -Fore Cyan
    Write-Host "`t`tEnvironment: " -NoNewline
    Write-Host "`t$SEL_ENV" -Fore Cyan
	Write-Host "`t`tBase Pack: " -NoNewline
	Write-Host "`t$BASE_PACK" -NoNewline -Fore Cyan
	Write-Host "`t-> (latest installed Full package)"
	Write-Host "`t`tPathcode Pack: " -NoNewline
	Write-Host "`t$PATH_PACK" -NoNewline -Fore Cyan
	Write-Host "`t-> (Pathcode specs are based on this Full package)"
    Write-Host "`t--------------------------------------------------------------------------------------------"
    Write-Host
    Write-Host "`t - Log Files from this session can be found at:"
    Write-Host "`t Standard logs (" -NoNewline
    Write-Host "jde.log, jdedbug.log" -NoNewline -fore Green
    Write-Host ") at:" 
    Write-Host "`t`t$LOGPATH" -Fore Cyan
    Write-Host "`t Localweb logs (" -NoNewline
    Write-Host "jas.log, e1root.log, rt.log, rtdebug.log" -NoNewline -Fore Green
    write-host ") at:" 
    Write-Host "`t`t$LOCALWEBLOGPATH" -Fore Cyan
    Write-Host "`t E1 Java logs (" -NoNewline
    Write-Host "e1jas.log, e1root.log" -NoNewline -Fore Green
    write-host ") at:" 
    Write-Host "`t`t$E1LOGPATH" -Fore Cyan
    if ($FULL_INTEROPINI) {
        Write-Host "`t BSSV logs (" -NoNewline
        write-host "jas.log, bssv.log, jderoot.log" -NoNewline -Fore Green
        write-host ") at:" 
        Write-Host "`t`t$BSSVLOGPATH" -Fore Cyan
    }
    write-host
    write-host "`t - Log Files from previous session are moved to:"
    Write-Host "`t`t$PREVLOGS" -Fore Cyan
    write-host
    if ($DIRORAINVDEL -eq "Y") {
        Write-Host "`t============================================================================================" -Fore Yellow
        write-host "`tDirectories under " -NoNewline
        write-host "$ORAINVBACKDIR\*" -NoNewline
        write-host " deleted!" -fore red
        Write-Host "`tSaved: "-NoNewline
        write-host "$DIRSIZE" -NoNewline -fore Green
        write-host " diskspace :-)" 
    }
    Write-Host "`t============================================================================================" -Fore Yellow
    Write-Host
    Write-Host
}

# :: FUNCTION delete Folders older than x days (with Progress bar)
# :: Param 1: folder to delete subfolders
# :: Param 2: nr days older that will be deleted
function deleteFolderswithProgress ($folderName, $limit) {
    $ListOfFiles = Get-ChildItem -Path $folderName | Where-Object {$_.PSIsContainer -and $_.CreationTime -lt $limit}
    #Write-Host "nrFiles: " + $ListOfFiles.count
    for ($i = 1; $i -lt $ListOfFiles.count; $i++) { 
        Write-Progress -Activity 'Deleting files...' -Status $ListOfFiles[$i].FullName -PercentComplete ($i / ($ListOfFiles.Count*100))
        Remove-Item -for -Path $ListOfFiles[$i].FullName -Confirm:$false -recurse #-WhatIf
    }
}

# :: FUNCTION delete Filess older than x days (with Progress bar)
# :: Param 1: folder where files will be deleted
# :: Param 2: nr days older that will be deleted
function deleteFileswithProgress ($folderName, $limit) {
    $ListOfFiles = Get-ChildItem -Path $folderName | Where-Object {!$_.PSIsContainer -and $_.CreationTime -lt $limit}
    #Write-Host "nrFiles: " + $ListOfFiles.count
    for ($i = 1; $i -lt $ListOfFiles.count; $i++) { 
        Write-Progress -Activity 'Deleting files...' -Status $ListOfFiles[$i].FullName -PercentComplete ($i / ($ListOfFiles.Count*100))
        Remove-Item -for -Path $ListOfFiles[$i].FullName -Confirm:$false -recurse #-WhatIf
    }
}

# --------------------------------------------------------
# -- Function section stops here
# --------------------------------------------------------
